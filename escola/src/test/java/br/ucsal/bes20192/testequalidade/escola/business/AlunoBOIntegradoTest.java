package br.ucsal.bes20192.testequalidade.escola.business;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private static Aluno aluno = AlunoBuilder.aluno().cujaMatriculaEh(200000000).cujoNomeEh("Astrobaldo Limberton")
			.cujaSituacaoEh(SituacaoAluno.ATIVO).cujoAnoNascimentoEh(2003).build();
	private static AlunoDAO dao = new AlunoDAO();
	private static AlunoBO bo = new AlunoBO(dao, new DateHelper());

	@Test
	public void testarCalculoIdadeAluno() {
		assertEquals(17, bo.calcularIdade(aluno.getMatricula()));
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {
		bo.atualizar(aluno);
		assertEquals(aluno, dao.encontrarPorMatricula(aluno.getMatricula()));
	}

}