package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

	private Aluno aluno;

	public static AlunoBuilder aluno() {
		return new AlunoBuilder();
	}

	public AlunoBuilder cujaMatriculaEh(Integer matricula) {
		aluno.setMatricula(matricula);
		return this;
	}

	public AlunoBuilder cujoNomeEh(String nome) {
		aluno.setNome(nome);
		return this;
	}

	public AlunoBuilder cujaSituacaoEh(SituacaoAluno situacao) {
		aluno.setSituacao(situacao);
		return this;
	}

	public AlunoBuilder cujoAnoNascimentoEh(Integer anoNascimento) {
		aluno.setAnoNascimento(anoNascimento);
		return this;
	}

	public Aluno build() {
		return aluno;
	}

}